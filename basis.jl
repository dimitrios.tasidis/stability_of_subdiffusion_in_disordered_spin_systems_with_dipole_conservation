#=
charge
input:  σ::Int = state
        L::Int = system size
output: Int: total charge of σ
=#
function charge(σ::Int, L::Int)
    return sum(digits(σ, base=3, pad=L) .- 1)
end


#=
dipole_moment
input:  σ::Int = state
        L::Int = system size
        n0::Int = reference site
output: Int: dipole moment of state σ calculated w.r.t. reference site n0
=#
function dipole_moment(σ::Int, L::Int, n0::Int)
    d = digits(σ, base=3, pad=L) .- 1
    p = 0
    for (n, s) in enumerate(d)
        p += (n - n0) * s
    end
    return p
end


#=
u1_basis
input:  L::Int = system size
        q::Int = total charge quantum number
output: Vector{Int}: basis states with charge quantum number q
=#
function u1_basis(L::Int, q::Int)
    b = Vector{Int}()
    for σ in 0:(3 ^ L - 1)
        if charge(σ, L) == q
            append!(b, σ)
        end
    end
    return b
end


#=
dipole_basis
input:  L::Int = system size
        q::Int = total charge quantum number
        p::Int = total dipole moment quantum number
        n0::Int = reference site
output: Vector{Int}: basis states for symmetry sector (q, p)
=#
function dipole_basis(L::Int, q::Int, p::Int, n0::Int)
    b = Vector{Int}()
    u1b = u1_basis(L, q)
    for σ in u1b
        if dipole_moment(σ, L, n0) == p
            append!(b, σ)
        end
    end
    return b
end
