function autocorrelator(method::String, model::String, L::Int, t_max::Int, n_timesteps::Int, w::Union{Float64, Int}, n_realizations::Int, bc::String, hamiltonian_path::String)
    # read or construct the hamiltonian
    h = read_hamiltonian(model, L, bc, hamiltonian_path)
    if h === nothing
        println("Generating Hamiltonian")
        h = save_hamiltonian(model, L, bc, hamiltonian_path)
        println("Done")
    end
    # spin observable in z direction for middle site
    if model != "feldmeier"
        sz = spinz(L, L÷2)
    else
        sz = spinz2(L, L÷2)
    end
    
    corrs = zeros(n_timesteps)
    if model != "feldmeier"
        b = Vector(0:(3^L-1))
    else
        b = Vector(0:(2^L-1))
    end
    for _ in 1:n_realizations
        w != 0 ? (hdisordered = add_disorder(h, b, L, w)) : hdisordered = h # add random disorder to clean hamiltonian

        if method == "lanczos"
            corrs += autocorrelation_time_evolution(sz, t_max, n_timesteps, hdisordered)
        elseif method == "ed"
            e, ψ = eigen(Hermitian(Array(hdisordered)))
            ψ = sparse(ψ)
            corrs += autocorrelation_time_evolution(sz, t_max, n_timesteps, e, ψ)
        end
    end
    corrs /= n_realizations

    return corrs
end
autocorrelator(method; model, L, t_max, n_timesteps, w, n_realizations, bc, hamiltonian_path, kwargs...) = autocorrelator(method, model, L, t_max, n_timesteps, w, n_realizations, bc, hamiltonian_path)


function plot_autocorrelator(label, method, model, L, bc, w, t_max, n_steps, n_realizations, autocorrelator_path, hamiltonian_path)
    plt = plot(scale=:log10, dpi=300)
    xlabel!(plt,"time \$ t \$")
    ylabel!(plt, "\$ \\langle S_{L/2}^z(t)S_{L/2}^z\\rangle\$")

    corr = read_autocorrelator(method, model, L, bc, w, t_max, n_steps, n_realizations, autocorrelator_path)
    if corr === nothing
        println("Calculating autocorrelators")
        corr = save_autocorrelator(method, model, L, bc, w, t_max, n_steps, n_realizations, autocorrelator_path, hamiltonian_path)
        println("Done")
    end
    if model == "feldmeier"
        corr = corr .- 1/4
    end
    if model in ["h3", "h4", "combined"]
        corr = corr .- 2 / (3 * L)
    end

    if method in ["ed", "lanczos"]
        t = range(0.01, t_max, n_steps) # logarithmic time scale ⇒ can't start at 0
    elseif method == "ca"
        t = range(1, t_max, t_max)
    end

    plot!(plt, t, abs.(real.(corr)), label=label)

    return plt
end
plot_autocorrelator(; label, method, model, L, bc, w, t_max, n_steps, n_realizations, autocorrelator_path, hamiltonian_path, kwargs...) = plot_autocorrelator(label, method, model, L, bc, w, t_max, n_steps, n_realizations, autocorrelator_path, hamiltonian_path)


function plot_autocorrelator!(plt, label, method, model, L, bc, w, t_max, n_steps, n_realizations, autocorrelator_path, hamiltonian_path)
    corr = read_autocorrelator(method, model, L, bc, w, t_max, n_steps, n_realizations, autocorrelator_path)
    if corr === nothing
        println("Calculating autocorrelators")
        corr = save_autocorrelator(method, model, L, bc, w, t_max, n_steps, n_realizations, autocorrelator_path, hamiltonian_path)
        println("Done")
    end
    if model == "feldmeier"
        corr = corr .- 1/4
    end
    if model in ["h3", "h4", "combined"]
        corr = corr .- 2 / (3 * L)
    end

    if method in ["ed", "lanczos"]
        t = range(0.01, t_max, n_steps) # logarithmic time scale ⇒ can't start at 0
    elseif method == "ca"
        t = range(1, t_max, t_max)
    end

    plot!(plt, t, real.(corr), label=label)

    return plt
end
plot_autocorrelator!(plt; label, method, model, L, bc, w, t_max, n_steps, n_realizations, autocorrelator_path, hamiltonian_path, kwargs...) = plot_autocorrelator!(plt, label, method, model, L, bc, w, t_max, n_steps, n_realizations, autocorrelator_path, hamiltonian_path)
