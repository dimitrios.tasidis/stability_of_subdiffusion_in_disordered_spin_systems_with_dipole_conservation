include("./deps.jl")

using SparseArrays
using HDF5
using LinearAlgebra
using Plots
using FlexiMaps
using Random
using Statistics
using KrylovKit
using Polynomials
using CurveFit
using LsqFit

include("./file_io.jl")
include("./helperfunctions.jl")
include("./hamiltonians.jl")
include("./krylov_spaces.jl")
include("./spin_matrices.jl")
include("./basis.jl")
include("./file_io_dipole_conserving_models.jl")
include("./autocorrelations.jl")
include("./time_evolution.jl")
include("./cellular_automata.jl")
include("./stochastic_automata.jl")
include("./scripts_fragmentation.jl")
include("./scripts_autocorrelations.jl")
