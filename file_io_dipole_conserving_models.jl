data() = "./data"


function hamiltonian_file(model, L, bc)
    return "$(model)_L$(L)_$(bc).h5"
end


function save_hamiltonian(model, L, bc, hamiltonian_path)
    file = hamiltonian_file(model, L, bc)
    if model == "h3"
        h = h3(L, bc)
    elseif model == "h4"
        h = h4(L, bc)
    elseif model == "combined"
        h = combined(L, bc)
    elseif model == "feldmeier"
        h = feldmeier(L, bc)
    end
    write_sparse(hamiltonian_path, file, "h", h)
    return h
end


function read_hamiltonian(model, L, bc, hamiltonian_path)
    file = hamiltonian_file(model, L, bc)
    h = read_sparse(hamiltonian_path, file, "h")
    return h
end


function krylov_space_file(model, L, bc, w)
    return "krylov_spaces_$(model)_L_$(L)_$(bc)_w_$(w).h5"
end


function save_krylov_spaces(model, L, bc, w, krylov_space_path, hamiltonian_path)
    file = krylov_space_file(model, L, bc, w)
    h = read_hamiltonian(model, L, bc, hamiltonian_path)
    if h === nothing
        h = save_hamiltonian(model, L, bc, hamiltonian_path)
    end
    h = add_disorder(h, Vector(0:(3^L - 1)), L, w)
    ks = all_krylov_spaces(h)
    d = size(ks, 1)
    write_variable(krylov_space_path, file, "d", d)
    for i in 1:d
        write_variable(krylov_space_path, file, "ks$i", ks[i])
    end
    return ks
end


function read_krylov_spaces(model, L, bc, w, krylov_space_path)
    file = krylov_space_file(model, L, bc, w)
    d = read_variable(krylov_space_path, file, "d")
    if d === nothing
        return nothing
    end
    ks = Vector()
    for i in 1:d
        push!(ks, read_variable(krylov_space_path, file, "ks$i"))
    end
    return ks
end


function autocorrelator_file(method::String, model::String, L::Int, bc::String, w::Int, t_max::Int, n_steps::Int, n_realizations::Int)
    return "autocorrelators_$(method)_$(model)_L$(L)_w_$(w)_tmax$(t_max)_nsteps$(n_steps)_nrealizations$(n_realizations)_bc$(bc).h5"
end


function save_autocorrelator(method, model, L, bc, w, t_max, n_steps, n_realizations, autocorrelator_path, hamiltonian_path)
    # n_steps are either the number of time steps in Lanczos or ED or the number of automaton runs
    file = autocorrelator_file(method, model, L, bc, w, t_max, n_steps, n_realizations)
    if method in ["ed", "lanczos"]
        corr = autocorrelator(method, model, L, t_max, n_steps, w, n_realizations, bc, hamiltonian_path)
    elseif method == "ca"
        if w == 0
            corr = autocorrelation_ca(model, L, t_max, n_steps, bc)
        end
    end
    write_variable(autocorrelator_path, file, "corr", corr)
    return corr
end


function read_autocorrelator(method, model, L, bc, w, t_max, n_steps, n_realizations, autocorrelator_path)
    file = autocorrelator_file(method, model, L, bc, w, t_max, n_steps, n_realizations)
    corr = read_variable(autocorrelator_path, file, "corr")
    return corr
end


function delete_autocorrelator(method, model, L, bc, w, t_max, n_steps, n_realizations, autocorrelator_path)
    file = autocorrelator_file(method, model, L, bc, w, t_max, n_steps, n_realizations)
    rm(joinpath(autocorrelator_path, file))
end
