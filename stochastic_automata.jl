#=
energy
input:  state::Vector{Int} = physical configuration
        disorder_landscape::Vector{Float64} = disorder value for each site
output: Float64: sum of disorder energy contributions on occupied sites
=#
function energy(state::Vector{Int}, disorder_landscape::Vector{Float64})
    return (state .- 1) ⋅ disorder_landscape
end

function autocorrelation_sca(model::String, L::Int, t_max::Int, n_runs::Int, bc::String, w::Union{Float64, Int}, n_realizations::Int, β::Union{Float64, Int})
    corr_average = zeros(t_max)
    model in ["feldmeier", "tb"] ? s = 1//2 : s = 1
    N = boundary(model, L, bc)
    for _ in 1:n_realizations
        disorder_landscape = w .* rand(L) .- w/2
        corr = zeros(t_max)
        for _ in 1:n_runs
            state = random_state(L, s)
            if state[L÷2] - floor(s) != 0
                spin_before = state[L÷2] - floor(s)
                corr[1] += 1
                for t in 2:t_max
                    state_before = copy(state)
                    for i in shuffle(1:N)
                        if model == "h3"
                            h3_automaton_rule(state, i)
                        elseif model == "h4"
                            h4_automaton_rule(state, i)
                        elseif model == "combined"
                            combined_automaton_rule(state, i, N)
                        elseif model == "feldmeier"
                            feldmeier_automaton_rule(state, i, N)
                        elseif model == "tb"
                            tb_automaton_rule(state, i)
                        end
                    end
                    ΔE = energy(state, disorder_landscape) - energy(state_before, disorder_landscape)
                    if ΔE > 0
                        if rand() > exp(-β * ΔE)
                            state = state_before
                        end
                    end

                    corr[t] += spin_before * (state[L÷2] - floor(s))
                end
            end
        end
        corr_average += corr / (n_realizations * n_runs)
    end
    return corr_average
end


function boltzmann_random_state(L, w, disorder_landscape, β)
    state = zeros(Int, L)
    i = 1
    min_energy = minimum(disorder_landscape)
    if min_energy > 0
        min_energy = 0
    end
    while i < L + 1
        proposed_spin = rand(-1:1)
        energy = proposed_spin * disorder_landscape[i]
        weight = exp(-β * energy) * exp(β * min_energy)
        # println("spin = $(proposed_spin), potential = $(disorder_landscape[i]), energy = $(energy)")
        # println(weight)
        if rand() < weight
            # println("accepted")
            state[i] = proposed_spin
            i += 1
        end
    end
    return state
end

function h3_initial_sampling(L, n_runs, w, n_realizations, β, bc)
    bc == "obc" ? N = L - 3 : N = L
    initialenergies = Vector{Float64}()

    # disorder realizations
    for _ in 1:n_realizations
        disorder_landscape = w .* rand(L) .- w / 2
        for _ in 1:n_runs
            state = boltzmann_random_state(L, w, disorder_landscape, β)
            push!(initialenergies, energy(state, disorder_landscape))
        end
    end

    number_energies = 100
    e_min = minimum(initialenergies)
    e_max = maximum(initialenergies)
    e_range = range(e_min, e_max, length=number_energies)
    hist = histogram(initialenergies, normalize=:pdf, bins=e_range, label="Distribution of initial states", dpi=300)
    energy_distance = (e_max - e_min) / number_energies
    plot!(hist, e_range, 1 / (energy_distance * sum(exp.(-β * e_range))) * exp.(-β * e_range), label="Boltzmann distribution", linewidth=5)
    xlabel!(hist, "Energy \$E\$")
    ylabel!(hist, "\$P(E)\$")
    return hist
end
    

function h3_confirm_sampling(L::Int, t_max::Int, n_runs::Int, w::Union{Float64, Int}, n_realizations::Int, β::Union{Float64, Int}, bc::String)
    bc == "obc" ? N = L - 3 : N = L
    s = 1
    energies = Vector{Float64}()
    initialenergies = Vector{Float64}()

    # disorder realizations
    for _ in 1:n_realizations
        disorder_landscape = w .* rand(L) .- w / 2
        n_rejected = 0
        for _ in 1:n_runs
            state = boltzmann_random_state(L, w, disorder_landscape, β)
            # state = random_state(L, s)
            # if rand() > exp(-β * energy(state, disorder_landscape))
            #     n_rejected += 1
            #     continue
            # end
            push!(initialenergies, energy(state, disorder_landscape))
            for _ in 1:t_max
                inds = shuffle(1:N)
                for i in inds
                    old_state = copy(state)
                    h3_automaton_rule(state, i)
                    ΔE = energy(state, disorder_landscape) - energy(old_state, disorder_landscape)
                    if ΔE > 0
                        if rand() > exp(-β * ΔE)
                            # don't apply update
                            state = copy(old_state)
                        end
                    end
                end # gates
                push!(energies, energy(state, disorder_landscape))
            end # time step
        end # automaton run
    end # disorder realization
    
    number_energies = 100
    e_min = minimum(energies)
    e_max = maximum(energies)
    e_range = range(e_min, e_max, length=number_energies)
    energy_distance = (e_max - e_min) / number_energies

    hist = histogram(energies, normalize=:pdf, bins=e_range, label="Simulation", dpi=300)
    plot!(hist, e_range, 1 / (energy_distance * sum(exp.(-β * e_range))) * exp.(-β * e_range), label="Boltzmann distribution", linewidth=5)
    xlabel!(hist, "Energy \$E\$")
    ylabel!(hist, "\$P(E)\$")
    return hist
end
