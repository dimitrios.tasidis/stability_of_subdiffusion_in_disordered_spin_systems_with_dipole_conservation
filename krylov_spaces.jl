#=
krylov_space
input:  r::Int = root state
        h::SparseMatrixCSC = hamiltonian
output: Vector{Int}: basis of krylov space
=#
function krylov_space(r::Int, h::SparseMatrixCSC)
    # represent state r as vector in hilbert space
    ϕ = spzeros(size(h,1))
    ϕ[r + 1] = 1

    k = Set(r)
    kold = Set()

    # construct krylov space by repeatedly applying hamiltonian until no new basis states get added
    while k != kold
        kold = copy(k)
        ϕ = h * ϕ
        for i in findnz(ϕ)[1]
            push!(k, i)
        end
    end

    return sort(collect(k))
end


#=
all_krylov_spaces
input:  h::SparseMatrixCSC = hamiltonian
output: Vector{Vector{Int}}: vector containing all krylov spaces
=#
function all_krylov_spaces(h::SparseMatrixCSC)
    ks = Vector{Vector{Int}}()
    check = Array(0:(size(h,1) - 1))

    # go through basis and construct krylov space for each state; remove states in krylov spaces from future root states since their krylov space is already found
    while check != []
        σ = check[1]
        k = krylov_space(σ, h)
        deleteat!(check, findall(x -> x in k, check))
        push!(ks, k)
    end
    return ks
end


#=
max_krylov_dim
input:  h::SparseMatrixCSC = hamiltonian
output: Int: dimension of biggest krylov space
=#
function max_krylov_dim(h::SparseMatrixCSC)
    ks = all_krylov_spaces(h)
    return maximum(length.(ks))
end


#=
fraction_max_krylov_hilbert
input:  h::SparseMatrixCSC = hamiltonian
output: Float64: fraction of dimensions of biggest krylov space and full hilbert space
=#
function fraction_max_krylov_hilbert(h::SparseMatrixCSC)
    hilbertdim = size(h,1)
    return max_krylov_dim(h) / hilbertdim
end
