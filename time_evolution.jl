#=
time_evolve (ED)
input:  ϕ::Vector = state
        t::Union{Int, Float64} = time
        Es::Vector = eigenenergies
        ψs::SparseMatrixCSC = eigenstates
output: Vector: state ϕ at time t
=#
function time_evolve(ϕ::Vector, t::Union{Int, Float64}, Es::Vector, ψs::SparseMatrixCSC)
    # construct time evolution operator 
    U = ψs * spdiagm(exp.(- 1im * Es * t)) * ψs'

    # schroedinger picture
    return U * ϕ
end


#=
time_evolve (Lanczos)
input:  ϕ::Vector = state
        t::Union{Int, Float64} = time
        h::SparseMatrixCSC = hamiltonian
output: Vector: state ϕ at time t
=#
function time_evolve(ϕ::Vector, t::Union{Int, Float64}, h::SparseMatrixCSC)
    return KrylovKit.exponentiate(h, -1im*t, ϕ; ishermitian=true)[1]
end
