#=
spinz
input:  L::Int = system size
        n::Int = site
output: SparseMatrixCSC: spin z at site n for a spin 1 system
=#
function spinz(L::Int, n::Int)
    return foldl(kron, fill(unity(), n-1)) ⊗ spinz() ⊗ foldl(kron, fill(unity(), L-n))
end

#=
spinz2
input:  L::Int = system size
        n::Int = site
output: SparseMatrixCSC: spin z at site n for a spin 1/2 system
=#
function spinz2(L::Int, n::Int)
    if n ∉ [1, L]
        return foldl(kron, fill(unity(2), n-1)) ⊗ [1 0; 0 -1] ⊗ foldl(kron, fill(unity(2), L-n))
    elseif n == 1
        return [1 0; 0 -1] ⊗ foldl(kron, fill(unity(2), L-1))
    else
        return foldl(kron, fill(unity(2), L-1)) ⊗ [1 0; 0 -1]
    end
end

#=
typical_state
input:  dim::Int = dimension of the hilbert space
output: Vector{ComplexF64}: typical state that can be used for dynamical typicality
=#
function typical_state(dim::Int)
    # random superposition of computational basis states
    ϕ = randn(ComplexF64, dim)

    # normalize
    return ϕ / norm(ϕ)
end


#=
autocorrelation_lanczos
input:  A::SparseMatrixCSC = observable
        t::Union{Float64, Int} = time
        ψ0::Union{Vector, SparseVector} = initial state
        φ0::Union{Vecotr, SparseVector} = observable * initial state
        h::SparseMatrixCSC = Hamiltonian
output: Vector{Float64}: evolution of the autocorrelation function of A up to t using Lanczos
=#
function autocorrelation_lanczos(A::SparseMatrixCSC, t::Union{Int, Float64}, ψ0::Union{Vector, SparseVector}, φ0::Union{Vector, SparseVector}, h::SparseMatrixCSC)
    # calculate autocorrelator using lanczos time evolution and
    # dynamical typicality: ⟨A(t)A⟩ = ⟨ϕ| exp(i h t) A exp(-i h t) A |ϕ⟩ = ⟨ψ(t)|A|φ(t)⟩
    ψt = time_evolve(ψ0, t, h)
    φt = time_evolve(φ0, t, h)
    corr = (ψt' * A * φt)
    return corr, ψt, φt
end


#=
autocorrelation
input:  A::SparseMatrixCSC = observable
        t::Union{Float64, Int} = time
        ϕ::Union{Vector, SparseVector} = initial state
        E::Vector = eigenenergies
        ψ::SparseMatrixCSC = eigenstates
output: Vector{Float64}: evolution of the autocorrelation function of A up to t using ED
=#
function autocorrelation(A::SparseMatrixCSC, t::Union{Int, Float64}, ϕ::Union{Vector, SparseVector}, E::Vector, ψ::SparseMatrixCSC)
    # calculate autocorrelator using time evolution by exact diagonalization and
    # dynamical typicality: ⟨A(t)A⟩ = ⟨ϕ| exp(i h t) A exp(-i h t) A |ϕ⟩ = ⟨ψ(t)|A|φ(t)⟩
    ψt = time_evolve(ϕ, t, E, ψ)
    φt = time_evolve(A*ϕ, t, E, ψ)
    corr = (ψt' * A * φt) / (ϕ' * ϕ)
    return corr
end


#=
autocorrelation_time_evolution
input:  A::SparseMatrixCSC = observable
        t_max::Union{Float64, Int} = maximal time up to which evolution runs
        n_timesteps::Int = number of timesteps to reach t_max
        h::SparseMatrixCSC = Hamiltonian
output: Vector{Float64}: evolution of the autocorrelation function of A up to t_max using Lanczos
=#
function autocorrelation_time_evolution(A::SparseMatrixCSC, t_max::Union{Float64, Int}, n_timesteps::Int, h::SparseMatrixCSC)
    autocorrelations = Vector()
    ϕ = typical_state(size(h, 1))

    ψ0 = ϕ
    φ0 = A*ϕ

    δt = t_max / n_timesteps
    for _ in 1:n_timesteps
        corr, ψ0, φ0 = autocorrelation_lanczos(A, δt, ψ0, φ0, h)
        append!(autocorrelations, corr)
    end
    return autocorrelations
end


#=
autocorrelation_time_evolution
input:  A::SparseMatrixCSC = observable
        t_max::Union{Float64, Int} = maximal time up to which evolution runs
        n_timesteps::Int = number of timesteps to reach t_max
        e::Vector = eigenenergies
        ψ::SparseMatrixCSC = eigenstates
output: Vector{Float64}: evolution of the autocorrelation function of A up to t_max using ED
=#
function autocorrelation_time_evolution(A::SparseMatrixCSC, t_max::Union{Float64, Int}, n_timesteps::Int, e::Vector, ψ::SparseMatrixCSC)
    autocorrelations = Vector()
    ϕ = typical_state(size(e, 1))

    for t in range(0, t_max, n_timesteps)
        corr = autocorrelation(A, t, ϕ, e, ψ)
        append!(autocorrelations, corr)
    end
    return autocorrelations
end
