function spinx()
    return 1 / √2 * sparse([0 1 0; 1 0 1; 0 1 0])
end


function spiny()
    return 1 / √2 * sparse([0 -1im 0; 1im 0 -1im; 0 1im 0])
end


function spinz()
    return sparse([1 0 0; 0 0 0; 0 0 -1])
end


function spinraise()
    return sparse([0 1 0; 0 0 1; 0 0 0])
end


function spinlower()
    return sparse([0 0 0; 1 0 0; 0 1 0])
end


unity(dim::Int=3) = sparse(I, dim, dim)
