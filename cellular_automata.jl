#=
random_state
input:  L::int = system size
        spin::Union{Rational, Int} = spin of the particles
output: Vector{Int}: random configuration of particles
=#
function random_state(L::Int, spin::Union{Rational, Int}=1)
    return rand(0:Int(2 * spin), L)
end


#=
boundary
input:  model::String
        L::Int = system size
        bc::String = boundary condition
output: Int: index of the rightmost lattice site where a local term can be applied while respecting the boundary condition
=#
function boundary(model::String, L::Int, bc::String)
    if bc == "obc"
        if model == "h3"
            N = L - 3
        elseif model == "h4"
            N = L - 4
        elseif model == "combined"
            N = L - 4
        elseif model == "feldmeier"
            N = L - 3
        elseif model == "tb"
            N = L - 1
        end
    elseif bc == "pbc"
        N = L
    end
    return N
end


#=
automaton_update!
input:  model::String 
        state::Vector = physical configuration
        i::Int = lattice site
        N::Int = boundary
output: nothing: updates given state
=#
function automaton_update!(state::Vector, model::String, i::Int, N::Int)
    if model == "h3"
        h3_automaton_rule(state, i)
    elseif model == "h4"
        h4_automaton_rule(state, i)
    elseif model == "combined"
        combined_automaton_rule(state, i, N)
    elseif model == "feldmeier"
        feldmeier_automaton_rule(state, i, N)
    elseif model == "tb"
        tb_automaton_rule(state, i)
    end
end


#=
autocorrelation_ca 
input:  model::String
        L::Int = system size
        t_max::Int = number of time steps
        n_runs::Int = number of automaton realizations
        bc::String = boundary condition
output: Vector: autocorrelation function for each timestep up to t_max
=#
function autocorrelation_ca(model::String, L::Int, t_max::Int, n_runs::Int, bc::String)
    corr = zeros(t_max)
    model in ["feldmeier", "tb"] ? s = 1//2 : s = 1 #spin
    N = boundary(model, L, bc)

    for _ in 1:n_runs
        # start with random state
        state = random_state(L, s)
        # if the spin in the middle of the initial state is 0 then the autocorrelation will be 0 for all times
        if state[L÷2] - floor(s) != 0
            spin_before = state[L÷2] - floor(s)
            corr[1] += 1
            for t in 2:t_max
                for i in shuffle(1:N)
                    # apply gate at site i
                    automaton_update!(state, model, i, N)
                end
                corr[t] += spin_before * (state[L÷2] - floor(s))
            end
        end
    end
    return corr / n_runs
end


#=
feldmeier_automaton_rule
input:  state::Vector{Int} = configuration of the physical system
        i::Int = lattice site where a local term gets applied
        N::Int = boundary
output: Vector: configuration after application of gate at site i
=#
function feldmeier_automaton_rule(state::Vector{Int}, i::Int, N::Int)
    L = length(state)
    possible_gates = Vector{Int}()
    # check whether h4 can be applied at site i
    if state[mod1(i, L)] != state[mod1(i+1, L)] && state[mod1(i+1, L)] == state[mod1(i+2, L)] && state[mod1(i+3, L)] == state[mod1(i, L)]
        push!(possible_gates, 1)
    end
    # check whether h5 can be applied at site i
    if i != N
        if state[mod1(i, L)] != state[mod1(i+1, L)] && state[mod1(i+1, L)] == state[mod1(i+3, L)] && state[mod1(i+4, L)] == state[mod1(i, L)]
            push!(possible_gates, 2)
        end
    end
    # out of the allowed transitions choose one at random
    !isempty(possible_gates) ? gate = rand(possible_gates) : gate = 0
        
    if gate == 1
        state[mod1(i, L)] = 1 - state[mod1(i, L)]
        state[mod1(i+1, L)] = 1 - state[mod1(i+1, L)]
        state[mod1(i+2, L)] = 1 - state[mod1(i+2, L)]
        state[mod1(i+3, L)] = 1 - state[mod1(i+3, L)]
    elseif gate == 2
        state[mod1(i, L)] = 1 - state[mod1(i, L)]
        state[mod1(i+1, L)] = 1 - state[mod1(i+1, L)]
        state[mod1(i+3, L)] = 1 - state[mod1(i+3, L)]
        state[mod1(i+4, L)] = 1 - state[mod1(i+4, L)]
    end
end
feldmeier_automaton_rule(i::Int; state::Vector{Int}, N::Int, kwargs...) = feldmeier_automaton_rule(state, i, N)


#=
h3_automaton_rule
input:  state::Vector{Int} = configuration of the physical system
        i::Int = lattice site where a local term gets applied
        N::Int = boundary
output: Vector: configuration after application of gate at site i
=#
function h3_automaton_rule(state::Vector{Int}, i::Int)
    L = length(state)
    possible_gates = Vector{Int}()
    # check whether H3 can be applied at site i
    if state[mod1(i, L)] != 2 && state[mod1(i + 1, L)] == 2 && state[mod1(i+2, L)] != 2
        push!(possible_gates, 1)
    end
    # check whether H3⁺ can be applied at site i
    if state[mod1(i, L)] != 0 && state[mod1(i + 1, L)] == 0 && state[mod1(i+2, L)] != 0
        push!(possible_gates, 2)
    end
    # out of the allowed transitions choose one at random
    !isempty(possible_gates) ? gate = rand(possible_gates) : gate = 0

    if gate == 1
        state[mod1(i, L)]    += 1
        state[mod1(i+1, L)]  -= 2
        state[mod1(i+2, L)]  += 1
    elseif gate == 2
        state[mod1(i, L)]    -= 1
        state[mod1(i+1, L)]  += 2
        state[mod1(i+2, L)]  -= 1
    end
end


#=
h4_automaton_rule
input:  state::Vector{Int} = configuration of the physical system
        i::Int = lattice site where a local term gets applied
        N::Int = boundary
output: Vector: configuration after application of gate at site i
=#
function h4_automaton_rule(state::Vector{Int}, i::Int)
    L = length(state)
    possible_gates = Vector{Int}()
    # check whether H4 can be applied at site i
    if state[mod1(i, L)] != 2 && state[mod1(i+1, L)] != 0 && state[mod1(i+2, L)] != 0 && state[mod1(i+3, L)] != 2
        push!(possible_gates, 1)
    end
    # check whether H4⁺ can be applied at site i
    if state[i] != 0 && state[mod1(i+1, L)] != 2 && state[mod1(i+2, L)] != 2 && state[mod1(i+3, L)] != 0
        push!(possible_gates, 2)
    end
    # out of the allowed transitions choose one at random
    !isempty(possible_gates) ? gate = rand(possible_gates) : gate = 0
    
    # apply chosen term
    if gate == 1
        state[mod1(i, L)]       += 1
        state[mod1(i+1, L)]     -= 1
        state[mod1(i+2, L)]     -= 1
        state[mod1(i+3, L)]     += 1
    elseif gate == 2
        state[mod1(i, L)]       -= 1
        state[mod1(i+1, L)]     += 1
        state[mod1(i+2, L)]     += 1
        state[mod1(i+3, L)]     -= 1
    end
end


#=
combined_automaton_rule
input:  state::Vector{Int} = configuration of the physical system
        i::Int = lattice site where a local term gets applied
        N::Int = boundary
output: Vector: configuration after application of gate at site i
=#
function combined_automaton_rule(state::Vector{Int}, i::Int, N::Int)
    L = length(state)
    possible_gates = Vector{Int}()
    # check whether H3 can be applied at site i
    if state[mod1(i, L)] != 2 && state[mod1(i + 1, L)] == 2 && state[mod1(i+2, L)] != 2
        push!(possible_gates, 1)
    end
    # check whether H3⁺ can be applied at site i
    if state[mod1(i, L)] != 0 && state[mod1(i + 1, L)] == 0 && state[mod1(i+2, L)] != 0
        push!(possible_gates, 2)
    end
    # check whether H4 can be applied at site i
    if i != N && state[mod1(i, L)] != 2 && state[mod1(i+1, L)] != 0 && state[mod1(i+2, L)] != 0 && state[mod1(i+3, L)] != 2
        push!(possible_gates, 3)
    end
    # check whether H4⁺ can be applied at site i
    if i != N && state[i] != 0 && state[mod1(i+1, L)] != 2 && state[mod1(i+2, L)] != 2 && state[mod1(i+3, L)] != 0
        push!(possible_gates, 4)
    end

    # out of the allowed transitions choose one at random
    !isempty(possible_gates) ? gate = rand(possible_gates) : gate = 0
    if gate == 1
        state[mod1(i, L)]    += 1
        state[mod1(i+1, L)]  -= 2
        state[mod1(i+2, L)]  += 1
    elseif gate == 2
        state[mod1(i, L)]    -= 1
        state[mod1(i+1, L)]  += 2
        state[mod1(i+2, L)]  -= 1
    elseif gate == 3
        state[mod1(i, L)]       += 1
        state[mod1(i+1, L)]     -= 1
        state[mod1(i+2, L)]     -= 1
        state[mod1(i+3, L)]     += 1
    elseif gate == 4
        state[mod1(i, L)]       -= 1
        state[mod1(i+1, L)]     += 1
        state[mod1(i+2, L)]     += 1
        state[mod1(i+3, L)]     -= 1
    end
end


#=
tb_automaton_rule
input:  state::Vector{Int} = configuration of the physical system
        i::Int = lattice site where a local term gets applied
        N::Int = boundary
output: Vector: configuration after application of gate at site i
=#
function tb_automaton_rule(state::Vector{Int}, i::Int)
    L = length(state)
    if state[mod1(i, L)] != state[mod1(i+1, L)]
        state[mod1(i, L)] = 1 - state[mod1(i, L)]
        state[mod1(i+1, L)] = 1 - state[mod1(i+1, L)]
    end
end
