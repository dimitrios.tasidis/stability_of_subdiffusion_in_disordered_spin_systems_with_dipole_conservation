⊗(a, b) = kron(a, b)


#=
h3
input:  L::Int = system size
        bc::String = boundary condition
output: SparseMatrixCSC: H₃ hamiltonian
=#
function h3(L::Int, bc::String)
    if bc == "obc"
        N = L - 2
    elseif bc == "pbc"
        N = L
    end

    ops = fill(unity(), L)
    ops[1] = spinraise()
    ops[2] = spinlower()^2
    ops[3] = spinraise()

    H = spzeros(3^L, 3^L)
    for _ in 1:N
        H -= foldl(kron, ops)
        circshift!(ops, 1)
    end
    return H + H'
end


#=
h4
input:  L::Int = system size
        bc::String = boundary condition
output: SparseMatrixCSC: H₄ hamiltonian
=#
function h4(L::Int, bc::String)
    if bc == "obc"
        N = L - 3
    elseif bc == "pbc"
        N = L
    end

    ops = fill(unity(), L)
    ops[1] = spinraise()
    ops[2] = spinlower()
    ops[3] = spinlower()
    ops[4] = spinraise()

    H = spzeros(3^L, 3^L)
    for _ in 1:N
        H -= foldl(kron, ops)
        circshift!(ops, 1)
    end
    return H + H'
end

function feldmeier(L::Int, bc::String)
    if bc == "obc"
        N = L - 4
    elseif bc == "pbc"
        N = L
    end

    ops = fill(unity(2), L)
    ops[1] = [0 1; 0 0]
    ops[2] = [0 0; 1 0]
    ops[3] = [0 0; 1 0]
    ops[4] = [0 1; 0 0]

    H4 = spzeros(2^L, 2^L)
    for _ in 1:N+1
        H4 -= foldl(kron, ops)
        circshift!(ops, 1)
    end
    H4 += H4'

    ops = fill(unity(2), L)
    ops[1] = [0 1; 0 0]
    ops[2] = [0 0; 1 0]
    ops[4] = [0 0; 1 0]
    ops[5] = [0 1; 0 0]
    H5 = spzeros(2^L, 2^L)
    for _ in 1:N
        H5 -= foldl(kron, ops)
        circshift!(ops, 1)
    end
    H5 += H5'

    return H4 + H5
end

#=
combined
input:  L::Int = system size
        bc::String = boundary condition
output: SparseMatrixCSC: H₃ + H₄
=#
function combined(L::Int, bc::String)
    return h3(L, bc) + h4(L, bc)
end


#=
symmetry_block
input:  h::SparseMatrixCSC = hamiltonian
        L::Int = system size
        q::Int = momentum
        p::Int = dipole moment
        n0::Int = reference site
output: SparseMatrixCSC: (q, p) - symmetry block of full hamiltonian
=#
function symmetry_block(h::SparseMatrixCSC, L::Int, q::Int, p::Int, n0::Int)
    # find states which belong to the symmetry sector
    b = dipole_basis(L, q, p, n0)
    dim = length(b)

    # only keep rows and columns of hamiltonian corresponding to states in symmetry sector
    h_block = sparse(reshape(h[b.+1, b.+1], (dim, dim)))
    return h_block
end


#=
tb
input:  L::Int = system size
        bc::String = boundary condition
output: SparseMatrixCSC: tight binding Hamiltonian
=#
function tb(L::Int, bc::String)
    if bc == "obc"
        N = L-1
    elseif bc == "pbc"
        N = L
    end

    ops = fill(unity(2), L)
    ops[1] = [0 1; 0 0]
    ops[2] = [0 0; 1 0]

    H = spzeros(2^L, 2^L)
    for _ in 1:N
        H -= foldl(kron, ops)
        circshift!(ops, 1)
    end
    return H + H'
end

#=
add_disorder
input:  h::SparseMatrixCSC = hamiltonian on which to add disorder
        b::Vector{Int} = basis
        L::Int = system size
        w::Union{Int, Float64} = disorder strength
output: SparseMatrixCSC: disordered hamiltonian
=#
function add_disorder(h::Union{SparseMatrixCSC{ComplexF64, Int}, SparseMatrixCSC{Float64, Int}, SparseMatrixCSC{Int, Int}}, b::Vector{Int}, L::Int, w::Union{Int, Float64})

    # disorder value for each site
    disorder_landscape = w * rand(Float64, L) .- w / 2

    # vector of disorder terms for basis states
    d = zeros(length(b))
    for (i, σ) in enumerate(b)
        a = digits(σ, base=2, pad=L) #for spin 1/2 Hamiltonians
        d[i] = a ⋅ disorder_landscape
    end

    # add disorder to Hamiltonian
    h_disordered = h + spdiagm(0 => d)
    return h_disordered
end
