function write_variable(path::String, file_name::String, variable_name::String, variable)
    mkpath(path)
    file = joinpath(path, file_name)
    if file_name ∈ readdir(path)
        h5open(file) do f
            if variable_name ∈ keys(f)
                rm(file, force=true)
            end
        end
    end
    h5write(file, variable_name, variable)
    return nothing
end


function write_sparse(path::String, file_name::String, variable_name::String, variable)
    mkpath(path)
    file = joinpath(path, file_name)
    if file_name ∈ readdir(path)
        h5open(file) do f
            if variable_name*"1" ∈ keys(f)
                rm(file, force=true)
            end
        end
    end
    y = findnz(variable)
    d = [x for x ∈ size(variable)]
    h5write(file, variable_name*"_dims", d)
    for i ∈ 1:length(y)
        h5write(file, variable_name*"$i", y[i])
    end
    return nothing
end


function read_variable(path::String, file_name::String, variable_name::String)
    x = nothing
    file = joinpath(path, file_name)
    if !isfile(file)
        return nothing
    end
    h5open(file) do f
        if variable_name ∉ keys(f)
            x = nothing
        else
            x = h5read(file, variable_name)
        end
    end
    return x
end


function read_sparse(path::String, file_name::String, variable_name::String)
    x = nothing
    file = joinpath(path, file_name)
    if !isfile(file)
        return nothing
    end
    d = h5read(file, variable_name*"_dims")
    y = Vector{Vector}()
    h5open(file) do f
        for i ∈ 1:(length(findall(x -> occursin(variable_name, x), keys(f))) - 1)
            push!(y, h5read(file, variable_name*"$i"))
        end
    end
    if length(y) > 2
        return sparse(y..., d...)
    elseif length(y) == 2
        return sparsevec(y..., d...)
    end
end


function read_sparse(path::String, file_name::String, variable_name::String, dims::Vector)
    x = nothing
    file = joinpath(path, file_name)
    if !isfile(file)
        return nothing
    end
    y = Vector{Vector}()
    h5open(file) do f
        for i ∈ 1:length(findall(x -> occursin(variable_name, x), keys(f)))
            push!(y, h5read(file, variable_name*"$i"))
        end
    end
    if length(y) > 2
        return sparse(y...)
    elseif length(y) == 2
        return sparsevec(y...)
    end
end

